﻿namespace SadPumpkin.Util.FilteringUtility.Property
{
    public interface IProperty
    {
        bool CanQuickFilter { get; }
    }
}