﻿namespace SadPumpkin.Util.CombatEngine.Item
{
    public enum ItemType
    {
        Invalid = 0,

        Weapon = 1,
        Armor = 2,
        Trinket = 4,
        Consumable = 8,
        Loot = 16
    }
}