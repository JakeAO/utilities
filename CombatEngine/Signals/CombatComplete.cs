﻿using SadPumpkin.Util.Signals;

namespace SadPumpkin.Util.CombatEngine.Signals
{
    public class CombatComplete : Signal<uint>
    {
        
    }
}