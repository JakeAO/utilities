﻿namespace SadPumpkin.Util.CombatEngine
{
    public interface IIdTracked
    {
        uint Id { get; }
    }
}