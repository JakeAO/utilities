namespace SadPumpkin.Util.CombatEngine
{
    public enum CombatState
    {
        Invalid = 0,
        Init = 1,
        Active = 2,
        Completed = 3
    }
}