﻿namespace SadPumpkin.Util.CombatEngine.EquipMap
{
    public enum EquipmentSlot
    {
        Weapon,
        Armor,
        ItemA,
        ItemB
    }
}